import os
import pytest

if __name__ == "__main__":
    path = os.path.abspath(os.path.dirname(os.getcwd()))
    pytest.main([
        # path + '\\testcases\\scenario_test\\test_01_register_login_list.py',
        # path + '\\testcases\\scenario_test\\test_02_register_login_update.py',
        # path + '\\testcases\\scenario_test\\test_03_register_login_delete.py',
        # path + '\\testcases\\api_test\\test_01_get_user_info.py',
        path + '\\testcases\\api_test\\test_03_login.py',
        # path + '\\TestProject\\HZXproject\\ReportCenter\\',
        # r'', '--env=phd_test_cq', '--alluredir', '../Log'])
        r'', '--alluredir', '../report'])
    os.system('allure generate ../report -o ../allure-report --clean')
