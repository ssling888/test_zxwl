INFO     log:test_01_get_user_info.py:52 *************** 开始执行用例 ***************
INFO     log:test_01_get_user_info.py:15 步骤1 ==>> 获取某个用户信息：ssl
INFO     log:rest_client.py:51 接口请求地址 ==>> http://10.0.0.104:30939/login/fromPC
INFO     log:rest_client.py:52 接口请求方式 ==>> POST
INFO     log:rest_client.py:54 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8"
}
INFO     log:rest_client.py:55 接口请求 params 参数 ==>> null
INFO     log:rest_client.py:56 接口请求体 data 参数 ==>> null
INFO     log:rest_client.py:57 接口请求体 json 参数 ==>> {
    "username": "ssl",
    "password": "000000"
}
INFO     log:rest_client.py:58 接口上传附件 files 参数 ==>> None
INFO     log:rest_client.py:59 接口 cookies 参数 ==>> null
INFO     log:user.py:122 登录用户 ==>> 返回结果 ==>> {"code":"200","msg":"操作成功","data":"bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAwNzY4MzYsImp0aSI6IjE1NjA2ODM4MTY2ODE1MTEifQ.EYzMsQjrvdFKfQ9TXc4IvI6twQ59AyFisGAuPNQbXpY","time":"2023-08-01 16:00:36"}
INFO     log:rest_client.py:51 接口请求地址 ==>> http://10.0.0.104:30939/user/page/list
INFO     log:rest_client.py:52 接口请求方式 ==>> POST
INFO     log:rest_client.py:54 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8",
    "Authorization": "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAwNzY4MzYsImp0aSI6IjE1NjA2ODM4MTY2ODE1MTEifQ.EYzMsQjrvdFKfQ9TXc4IvI6twQ59AyFisGAuPNQbXpY"
}
INFO     log:rest_client.py:55 接口请求 params 参数 ==>> null
INFO     log:rest_client.py:56 接口请求体 data 参数 ==>> null
INFO     log:rest_client.py:57 接口请求体 json 参数 ==>> {
    "pageNum": 1,
    "pageSize": 10,
    "keyword": "ssl",
    "organizationId": "1"
}
INFO     log:rest_client.py:58 接口上传附件 files 参数 ==>> None
INFO     log:rest_client.py:59 接口 cookies 参数 ==>> null
INFO     log:user.py:61 查看单个用户 ==>> 返回结果 ==>> {"code":"200","msg":"操作成功","data":{"records":[{"createTime":"2023-04-25 11:02:30","creatorName":"admin","updateTime":"2023-05-24 13:59:20","lastUpdateName":"admin","lastLoginTime":null,"id":"1542889301606432","operateType":0,"disabled":0,"position":null,"systemType":null,"loginName":"ssl","employeeCode":null,"name":"宋叔玲","mobile":"18280387084","affiliation":null,"commonStatus":1,"organizationList":[{"id":"1","name":"川威集团","userId":"1542889301606432"}],"roleList":[{"userId":"1542889301606432","roleId":"2","roleName":"客户"}]}],"total":"1","size":"10","current":"1","orders":[],"optimizeCountSql":true,"searchCount":true,"countId":null,"maxLimit":null,"pages":"1"},"time":"2023-08-01 16:00:36"}
INFO     log:test_01_get_user_info.py:58 code ==>> 期望结果：200， 实际结果：【 200 】