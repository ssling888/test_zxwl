[2023-08-01 16:00:37,404][test_01_get_user_info.py 32][INFO]: *************** 开始执行用例 ***************
[2023-08-01 16:00:37,405][test_01_get_user_info.py 10][INFO]: 步骤1 ==>> 获取所有用户信息
[2023-08-01 16:00:37,405][rest_client.py 51][INFO]: 接口请求地址 ==>> http://10.0.0.104:30939/login/fromPC
[2023-08-01 16:00:37,405][rest_client.py 52][INFO]: 接口请求方式 ==>> POST
[2023-08-01 16:00:37,405][rest_client.py 54][INFO]: 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8"
}
[2023-08-01 16:00:37,405][rest_client.py 55][INFO]: 接口请求 params 参数 ==>> null
[2023-08-01 16:00:37,405][rest_client.py 56][INFO]: 接口请求体 data 参数 ==>> null
[2023-08-01 16:00:37,405][rest_client.py 57][INFO]: 接口请求体 json 参数 ==>> {
    "username": "ssl",
    "password": "000000"
}
[2023-08-01 16:00:37,405][rest_client.py 58][INFO]: 接口上传附件 files 参数 ==>> None
[2023-08-01 16:00:37,405][rest_client.py 59][INFO]: 接口 cookies 参数 ==>> null
[2023-08-01 16:00:37,591][user.py 122][INFO]: 登录用户 ==>> 返回结果 ==>> {"code":"200","msg":"操作成功","data":"bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAwNzY4MzUsImp0aSI6IjE1NjA2ODM4MTQ1ODQzNTkifQ.osLNtTMkqGYdaBIlPv7jHaiG8OzfsuVorcR5TNGs_vQ","time":"2023-08-01 16:00:35"}
[2023-08-01 16:00:37,591][rest_client.py 51][INFO]: 接口请求地址 ==>> http://10.0.0.104:30939/user/page/list
[2023-08-01 16:00:37,591][rest_client.py 52][INFO]: 接口请求方式 ==>> POST
[2023-08-01 16:00:37,591][rest_client.py 54][INFO]: 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8",
    "Authorization": "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAwNzY4MzUsImp0aSI6IjE1NjA2ODM4MTQ1ODQzNTkifQ.osLNtTMkqGYdaBIlPv7jHaiG8OzfsuVorcR5TNGs_vQ"
}
[2023-08-01 16:00:37,591][rest_client.py 55][INFO]: 接口请求 params 参数 ==>> null
[2023-08-01 16:00:37,591][rest_client.py 56][INFO]: 接口请求体 data 参数 ==>> null
[2023-08-01 16:00:37,591][rest_client.py 57][INFO]: 接口请求体 json 参数 ==>> {
    "pageNum": 1,
    "pageSize": 10,
    "keyword": "",
    "organizationId": "1"
}
[2023-08-01 16:00:37,591][rest_client.py 58][INFO]: 接口上传附件 files 参数 ==>> None
[2023-08-01 16:00:37,591][rest_client.py 59][INFO]: 接口 cookies 参数 ==>> null
[2023-08-01 16:00:37,644][test_01_get_user_info.py 38][INFO]: code ==>> 期望结果：200， 实际结果：200
[2023-08-01 16:00:37,645][test_01_get_user_info.py 41][INFO]: *************** 结束执行用例 ***************
