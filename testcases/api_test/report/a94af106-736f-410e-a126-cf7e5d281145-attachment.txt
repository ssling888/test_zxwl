INFO     log:test_01_get_user_info.py:55 *************** 开始执行用例 ***************
INFO     log:conftest.py:40 前置步骤 ==>> 管理员  登录，返回信息 为：bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAxNTcxNjYsImp0aSI6IjE1NjA4NTIyODA5MDE2NjQifQ.fd6txn-RJS8UUc01rk0LGM4wzVzttP0Jvq3eEhp5ieo
INFO     log:test_01_get_user_info.py:10 步骤1 ==>> 获取所有用户信息
INFO     log:rest_client.py:51 接口请求地址 ==>> http://10.0.0.104:30939/user/page/list
INFO     log:rest_client.py:52 接口请求方式 ==>> POST
INFO     log:rest_client.py:54 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8",
    "Authorization": "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAxNTcxNjYsImp0aSI6IjE1NjA4NTIyODA5MDE2NjQifQ.fd6txn-RJS8UUc01rk0LGM4wzVzttP0Jvq3eEhp5ieo"
}
INFO     log:rest_client.py:55 接口请求 params 参数 ==>> null
INFO     log:rest_client.py:56 接口请求体 data 参数 ==>> null
INFO     log:rest_client.py:57 接口请求体 json 参数 ==>> {
    "pageNum": 1,
    "pageSize": 10,
    "keyword": "wintest1111",
    "organizationId": "1"
}
INFO     log:rest_client.py:58 接口上传附件 files 参数 ==>> None
INFO     log:rest_client.py:59 接口 cookies 参数 ==>> null
INFO     log:user.py:64 查看单个用户 ==>> 返回结果 ==>> {"code":"200","msg":"操作成功","data":{"records":[],"total":"0","size":"10","current":"1","orders":[],"optimizeCountSql":true,"searchCount":true,"countId":null,"maxLimit":null,"pages":"0"},"time":"2023-08-02 14:19:27"}
INFO     log:test_01_get_user_info.py:64 code ==>> 期望结果：200， 实际结果：【 200 】
INFO     log:test_01_get_user_info.py:68 *************** 结束执行用例 ***************