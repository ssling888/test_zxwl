INFO     log:test_01_get_user_info.py:32 *************** 开始执行用例 ***************
INFO     log:test_01_get_user_info.py:10 步骤1 ==>> 获取所有用户信息
INFO     log:rest_client.py:51 接口请求地址 ==>> http://10.0.0.104:30939/login/fromPC
INFO     log:rest_client.py:52 接口请求方式 ==>> POST
INFO     log:rest_client.py:54 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8"
}
INFO     log:rest_client.py:55 接口请求 params 参数 ==>> null
INFO     log:rest_client.py:56 接口请求体 data 参数 ==>> null
INFO     log:rest_client.py:57 接口请求体 json 参数 ==>> {
    "username": "ssl",
    "password": "000000"
}
INFO     log:rest_client.py:58 接口上传附件 files 参数 ==>> None
INFO     log:rest_client.py:59 接口 cookies 参数 ==>> null
INFO     log:user.py:111 登录用户 ==>> 返回结果 ==>> {"code":"200","msg":"操作成功","data":"bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAwNzU3ODMsImp0aSI6IjE1NjA2ODE2MDgzODA0NTUifQ.QSyhFTXN2yLwTd_qVtXP8MESV3x8SkF-WTUFRA97yuU","time":"2023-08-01 15:43:03"}
INFO     log:rest_client.py:51 接口请求地址 ==>> http://10.0.0.104:30939/user/page/list
INFO     log:rest_client.py:52 接口请求方式 ==>> POST
INFO     log:rest_client.py:54 接口请求头 ==>> {
    "Content-Type": "application/json; charset=UTF-8",
    "Authorization": "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjb21wYW55cyI6WyJBM29BQUFBQW1XKy9EQVFPIl0sImxvZ2luTmFtZSI6InNzbCIsInJvbGVzIjpbMl0sIm5hbWUiOiLlrovlj5TnjrIiLCJtb2JpbGUiOiIxODI4MDM4NzA4NCIsImlkIjoxNTQyODg5MzAxNjA2NDMyLCJleHAiOjE5NTAwNzU3ODMsImp0aSI6IjE1NjA2ODE2MDgzODA0NTUifQ.QSyhFTXN2yLwTd_qVtXP8MESV3x8SkF-WTUFRA97yuU"
}
INFO     log:rest_client.py:55 接口请求 params 参数 ==>> null
INFO     log:rest_client.py:56 接口请求体 data 参数 ==>> null
INFO     log:rest_client.py:57 接口请求体 json 参数 ==>> {
    "pageNum": 1,
    "pageSize": 10,
    "keyword": "",
    "organizationId": "1"
}
INFO     log:rest_client.py:58 接口上传附件 files 参数 ==>> None
INFO     log:rest_client.py:59 接口 cookies 参数 ==>> null
INFO     log:test_01_get_user_info.py:38 code ==>> 期望结果：0， 实际结果：200