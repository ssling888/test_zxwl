#!/usr/bin/python
# coding=utf-8
import hashlib
import os
import yaml
import json
import time
import sys
import pymysql
import redis
from common.logger import logger
import random


# 获取随机范围数字
def get_random(param1, param2):
    return str(random.randint(int(param1), int(param2)))


# 读取yaml文件数据
def _base_data(file_name):
    last_path = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))  # 读取当前目录的上一级目录
    print(last_path)
    cur_path = os.path.join(last_path, "testcase")  # Config目录的路径
    fs = os.path.join(cur_path, file_name)  # 获取到要读取的文件
    f1 = open(fs, 'r', encoding='utf-8', errors='ignore')  # 将读取到的文件
    data = yaml.load(f1, Loader=yaml.FullLoader, )
    return data


# 读取yaml文件数据(根据yamlkey获取对应值)
def get_test_data(filepath, yamlkey=""):
    path = filepath
    with open(path, encoding='utf-8', mode='r', errors='ignore') as f:
        # data = yaml.load(f.read(), Loader=yaml.SafeLoader)
        data = yaml.safe_load(f.read())
        if yamlkey == "":
            return data
        return data.get(yamlkey)


# 获取yaml文件数据（同_base_data）
def get_base_data(filename=None):
    '''
    :param filename:   yaml格式的文件， 文件名必须以.yml结尾
    :return: 返回的是字典
    '''
    if filename != None:
        if filename.endswith('.yml') or filename.endswith('.yaml'):
            base_data = _base_data(filename)
            # base_data = get_test_data(filename,0)
            return base_data
        else:
            print("filename 必须以.yml结尾")
            raise Exception('filename 必须以.yml结尾')
    else:
        raise Exception('filename 不能为空，缺失参数，文件名以.yml结尾')


# 获取Json数据中，key对应的值
def josnFind(jsonb, key):
    for k in jsonb:
        print(k, jsonb[k])
        if k == key:
            return jsonb[k]
        chlidType = type(jsonb[k])
        if dict == chlidType:
            return josnFind(jsonb[k], key)


# 通过比对校验数据正确性
def check_expect_info(dict1, dict2):
    for i in str(dict1).replace('"', "'").replace(" ", "")[1:-1].split(','):
        if i in str(dict2).replace('"', "'").replace(" ", ""):
            print("字段：", i, "校验通过")
        else:
            assert False, '字段校验失败:' + i


def md5Encryption(pwd=None, *args, **kwargs):
    """
    :param args:  任意字符串
    :param kwargs: 字典
    :return:   (['dd93ccc1be354c2d3799fd0f9ebdf0ef'], {'mima': 'e10adc3949ba59abbe56e057f20f883e'})
    :例如  md5Encryption('pwx618302',mima='123456')
    """
    if pwd != None:
        pwd = hashlib.md5(bytes(pwd, encoding='utf-8')).hexdigest()

    str_md5 = []
    for item in args:
        # pyhton3 默认的str是Unicode，
        str_md5.append(hashlib.md5(bytes(item, encoding='utf-8')).hexdigest())
    for k, v in kwargs.items():
        kwargs[k] = hashlib.md5(bytes(v, encoding='utf-8')).hexdigest()
    if len(args) > 0 and len(kwargs) > 0 and pwd != None:
        return pwd, str_md5, kwargs
    elif len(args) > 0 and len(kwargs) == 0 and pwd != None:
        return pwd, str_md5
    elif len(args) == 0 and len(kwargs) > 0 and pwd != None:
        return pwd, kwargs
    elif len(args) == 0 and len(kwargs) == 0 and pwd != None:
        return pwd
    else:
        raise Exception("请传入要加密的字符串")


def json_format(jsons):
    return json.dumps(jsons, sort_keys=True, indent=4, separators=(',', ':'), ensure_ascii=False)


if __name__ == '__main__':
    pwd = md5Encryption("123456ling", mima="123456ling")
    print(pwd)


def get_now_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())


def get_case_name():
    return sys._getframe().f_code.co_name


def connection_mysql(db=None,
                     host=None,
                     user=None,
                     passwd=None,
                     port=None,
                     sql_type=None, *args):
    """
    :param db:  数据库的数据库，可以为None，选填
    :param host: 数据库ip，必填
    :param user: mysql的数据库用户名
    :param passwd: mysql的数据库密码
    :param port:  mysql的端口号
    :param sql_type:  sql语句的类型 insert，delect ，select
    :param args:   sql语句
    :return:
    eg      Util.connection_mysql('hzx-research',
                             '192.168.0.18',
                             'root',
                             'myPassswd123',
                              3313,
                             item_v['sql_type'], sql_i)
    """
    # db = pymysql.connect("localhost", "testuser", "test123", "TESTDB", charset='utf8')
    # 连接mysql数据库
    connection = pymysql.connect(
        db=db,
        host=host,
        user=user,
        passwd=passwd,
        port=port,
        # charset="utf-8",
    )
    c = connection.cursor()
    for item in args:
        # 执行sql语句
        c.execute(item)
        if sql_type == 'select':
            try:
                logger.info("【查询结果】{0}".format(c.fetchall()))
            except:
                logger.info("查询失败")
        else:
            try:
                # 提交给数据库执行命令
                connection.commit()
            except:
                # 回滚，以防出现错误
                logger.info("操作失败")
    c.close()
    connection.close()


def connection_redis(host=None, password=None, port=None, db=None, decode_responses=False
                     ):
    '''

    :param host:  redis的ip
    :param password: redis的密码
    :param port: redis的端口号
    :param db: redis的数据库    # redis默认连接db0
    :param decode_responses: 编码 ，默认是FASLE
    :return: 返回连接状态
    '''

    conn = redis.Redis(host=host,
                       password=password,
                       port=port,
                       db=db,
                       decode_responses=decode_responses)
    return conn


def get_vcode(to, type, evn_config):
    """
    目前仅用于获取汇智信app端和manager端的验证码
    :param to:  token
    :param  type ：  app    hzx-app-validate-code:5087d9a85d094407a6c5b008582ac273
    :return:    get_vcode(dict_info['validateCode'])  验证码
    例如
    url = 'https://hzx-rec-test.xyebank.com:10443/hzx-mobile/validateCode'
    res = requests.request(method='GET', url=url, cookies=None)
    dict_info['validateCode'] =res.headers['Token']   # token
    get_vcode(dict_info['validateCode'],"app")
    """

    conn = connection_redis(host=evn_config['redis_cfg']['host'],
                            password=evn_config['redis_cfg']['password'],
                            port=evn_config['redis_cfg']['port'],
                            db=evn_config['redis_cfg']['db'],
                            decode_responses=evn_config['redis_cfg']['decode_responses'])

    r_k = 'hzx-{0}-validate-code:{1}'.format(type, to)

    print(r_k)
    print(conn[r_k])
    validatecode = conn[r_k][-4:].decode('utf-8')
    print(validatecode)
    # print(type(validatecode))
    return validatecode


def exc_sql(file, evn_config, format_params=None):
    print('{0}以下是sql执行{0}'.format("*" * 30))
    for item_k, item_v in file.items():
        logger.info('【sql_name】{0}'.format(item_v['sql_name']))
        logger.info(item_v['sql_name'])
        logger.info('【sql_func】{0}'.format(item_v['sql_func']))
        logger.info(item_v['sql_func'])
        logger.info('【sql_table】{0}'.format(item_v['sql_table']))
        try:
            sql_i = "{0}".format(item_v['exc_sql']).format(format_params)
        except:
            sql_i = "{0}".format(item_v['exc_sql'])
        logger.info(sql_i)
        connection_mysql(evn_config['mysql_cfg']['db'],
                         evn_config['mysql_cfg']['host'],
                         evn_config['mysql_cfg']['user'],
                         evn_config['mysql_cfg']['passwd'],
                         evn_config['mysql_cfg']['port'],
                         item_v['sql_type'], sql_i)

# if __name__=="__main__":
#     hzx_config={"redis_cfg": {'host': '192.168.0.18',
#                   'password': '',
#                   'port': 6482,
#                   'db': 5,
#                   'decode_responses': False
#                   }}
#     import requests
#
#     dict_info={}
#     url = 'https://hzx-rec-test.xyebank.com:10443/hzx-mobile/validateCode'
#     res = requests.request(method='GET', url=url, cookies=None)
#     print(res)
#     dict_info['validateCode'] =res.headers['Token']
#     print(dict_info['validateCode'])
#     get_vcode(dict_info['validateCode'],"app",hzx_config)
#     phd_config = {
#         "mysql_cfg": {"db": "wulianb",
#                       "host": '10.0.0.111',
#                       'user': 'wulianb',
#                       'passwd': 'wulianb@123',
#                       'port': 30306
#                       },
#         "url": {"c_url": "https://jf-xyarch-test-cq.xycredit.com.cn:10443/api",
#                 "b_url": 'https://jf-xyarch-mgr-test-cq.xycredit.com.cn:10443/api'
#                 }
#     }
#     from TestProject.Mallproject.common_params import common_params
#     sql_dict_yaml=get_base_data("Mallproject/user/user_sql.yaml")
#     exc_sql(sql_dict_yaml,phd_config,format_params=common_params['account'])
#
