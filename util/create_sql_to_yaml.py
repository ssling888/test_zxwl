# -*- coding:utf-8 -*-
import pandas as pd
import yaml
import os


def create_sql_yaml():
    path = os.path.abspath(os.path.dirname(os.getcwd()))
    excel_name = "hzx_manager_api.xlsx"

    df = pd.read_excel(path + r"\data\%s" % excel_name, sheet_name="hzx_sql", na_values=['N/A', 'NA', '#NA', '-NaN'],
                       keep_default_na=False)
    df.set_index("sql_id", inplace=True)
    df_dict = df.to_dict('index')

    with open(path + r'\TestProject\HZXproject\Manager\Manager_sql.yaml', "w", encoding="utf-8") as f:
        yaml.dump(df_dict, f, Dumper=yaml.Dumper, default_flow_style=False, allow_unicode=True, indent=4)
