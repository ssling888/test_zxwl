import allure
from core.result_base import ResultBase
from api.user import user
from common.logger import logger


def get_all_user_info(token):
    """
    获取全部用户信息
    :return: 自定义的关键字返回结果 result
    """
    result = ResultBase()
    header = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": token
    }
    json_data = {
        "pageNum": 1,
        "pageSize": 10,
        "keyword": "",
        "organizationId": "1"
    }
    res = user.list_all_users(json=json_data, headers=header)
    result.success = False
    if res.json()["code"] == "200":
        result.success = True
    else:
        result.error = "接口返回码是 【 {} 】, 返回信息：{} ".format(res.json()["code"], res.json()["msg"])
    result.msg = res.json()["msg"]
    result.response = res
    with allure.step("请求接口：%s,请求方法：%s,请求头：%s" % ("list_all_users", "POST", header)):
        # allure.attach("接口用例描述：", "{0}".format(get_case_name))
        allure.attach("接口用例请求参数：", "{0}".format(json_data))
    return result


def get_one_user_info(username, token):
    """
    获取单个用户信息
    :param token: 鉴权信息
    :param username:  用户名
    :return: 自定义的关键字返回结果 result
    """
    result = ResultBase()
    result.success = False
    header = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": token
    }
    json_data = {
        "pageNum": 1,
        "pageSize": 10,
        "keyword": username,
        "organizationId": "1"
    }
    res = user.list_all_users(json=json_data, headers=header)
    if res.json()["code"] == "200":
        result.success = True
    else:
        result.error = "查询用户 ==>> 接口返回码是 【 {} 】, 返回信息：{} ".format(res.json()["code"], res.json()["msg"])
    result.msg = res.json()["msg"]
    result.response = res
    result.data = res.json()["data"]["records"]
    logger.info("查看单个用户 ==>> 返回结果 ==>> {}".format(result.response.text))
    with allure.step("请求接口：%s,请求方法：%s,请求头：%s" % ("list_all_users", "POST", header)):
        # allure.attach("接口用例描述：", "{0}".format(get_case_name))
        allure.attach("接口用例请求参数：", "{0}".format(json_data))
    return result


def add_user(token, username, password, telephone, disabled=0, roleIdList="1", organizationIdList="1", supAnalysisCode=''):
    """
    注册用户信息
    :param token: 鉴权信息
    :param disabled: 是否禁用（0-否  1-是）
    :param username: 用户名
    :param password: 密码
    :param telephone: 手机号
    :param roleIdList: 角色
    :param organizationIdList: 组织机构
    :param supAnalysisCode: 客商分析码
    :return: 自定义的关键字返回结果 result
    """
    result = ResultBase()
    json_data = {
      "id": id,
      "name": username,
      "mobile": telephone,
      "disabled": disabled,
      "loginName": username,
      "position": '',
      "roleIdList": [
          roleIdList
      ],
      "organizationIdList": [
        organizationIdList
      ],
      "supAnalysisCode": supAnalysisCode,
      "password": password
    }
    header = {
        "Content-Type": "application/json; charset=UTF-8",
        "Authorization": token
    }
    res = user.add_or_update(json=json_data, headers=header)
    result.success = False
    if res.json()["code"] == "200":
        result.success = True
    else:
        result.error = "接口返回码是 【 {} 】, 返回信息：{} ".format(res.json()["code"], res.json()["msg"])
    result.msg = res.json()["msg"]
    result.response = res
    logger.info("注册用户 ==>> 返回结果 ==>> {}".format(result.response.text))
    return result


def login_user(username, password):
    """
    登录用户
    :param username: 用户名
    :param password: 密码
    :return: 自定义的关键字返回结果 result
    """
    result = ResultBase()
    code_res = user.getcode()
    code = code_res.json()["data"]["value"]
    sessionId = code_res.json()["data"]["sessionId"]
    data = {
        "grant_type": 'password',
        "scope": 'server',
        "kaptcha": code,
        "sessionId": sessionId,
        "username": username,
        "password": password
    }
    # grant_type = password & scope = server & kaptcha = fmny5 & sessionId = 4
    # D6948A8EECCC7B90951511127781ADA & username = 18280387084 & password = S18280387084
    header = {
        # "Content-Type": "application/json; charset=UTF-8"
        "Authorization": "Basic bWJjbG91ZDptYmNsb3Vk"
    }
    res = user.login(params=data, headers=header)
    result.success = False
    print(data, '---------------------------------------------', res.json())
    if res.json()["code"] == "0":
        result.success = True
        result.token = res.json()["data"]
    else:
        result.error = "接口返回码是 【 {} 】, 返回信息：{} ".format(res.json()["code"], res.json()["msg"])
    result.msg = res.json()["msg"]
    result.response = res
    logger.info("登录用户 ==>> 返回结果 ==>> {}".format(result.response.text))
    with allure.step("请求接口：%s,请求方法：%s,请求头：%s" % ("list_all_users", "POST", header)):
        # allure.attach("接口用例描述：", "{0}".format(get_case_name))
        allure.attach("接口用例请求参数：", "{0}".format(data))
    return result


def delete_user(username, admin_user, token):
    """
    根据用户名，删除用户信息
    :param username: 用户名
    :param admin_user: 当前操作的管理员用户
    :param token: 当前管理员用户的token
    :return: 自定义的关键字返回结果 result
    """
    result = ResultBase()
    json_data = {
        "admin_user": admin_user,
        "token": token,
    }
    header = {
        "Content-Type": "application/json"
    }
    res = user.delete(username, json=json_data, headers=header)
    result.success = False
    if res.json()["code"] == 0:
        result.success = True
    else:
        result.error = "接口返回码是 【 {} 】, 返回信息：{} ".format(res.json()["code"], res.json()["msg"])
    result.msg = res.json()["msg"]
    result.response = res
    logger.info("删除用户 ==>> 返回结果 ==>> {}".format(result.response.text))
    return result
